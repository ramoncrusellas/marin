<?php

/*
* Create the custom post type
*/

function custom_post_type_cole() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Projects', 'Post Type General Name', 'twentythirteen' ),
        'singular_name'       => _x( 'Project', 'Post Type Singular Name', 'twentythirteen' ),
        'menu_name'           => __( 'Projects', 'twentythirteen' ),
        'parent_item_colon'   => __( 'Parent Movie', 'twentythirteen' ),
        'all_items'           => __( 'All projects', 'twentythirteen' ),
        'view_item'           => __( 'See projects', 'twentythirteen' ),
        'add_new_item'        => __( 'Add project', 'twentythirteen' ),
        'add_new'             => __( 'Add', 'twentythirteen' ),
        'edit_item'           => __( 'Modify project', 'twentythirteen' ),
        'update_item'         => __( 'Update project', 'twentythirteen' ),
        'search_items'        => __( 'Find project', 'twentythirteen' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'projects', 'twentythirteen' ),
        'description'         => __( 'Project', 'twentythirteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'tag', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies' => array('category'),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 4,
        'menu_icon' => 'dashicons-images-alt',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        /*'rewrite' => array(
            'slug' => 'movies'
        )*/
    );

    // Registering your Custom Post Type
    register_post_type( 'Projects', $args );

}

add_action( 'init', 'custom_post_type_cole', 0 );

// Custom Taxonomy

