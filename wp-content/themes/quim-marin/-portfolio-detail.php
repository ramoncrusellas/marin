<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Quim Marin Studio</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- bootstrap CSS -->
        <link rel="stylesheet" href="css/includes/bootstrap/bootstrap.min.css">

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/includes/grid-loading/component.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


       <header>

           <a href="index.php"><div class="brand">Quim Marin<br>Studio</div></a>

           <div class="menu">
               <ul>
                   <li><a href="#">Identity</a></li>
                   <li><a href="#">Posters</a></li>
                   <li><a href="#">Design</a></li>
                   <li><a href="#">Contact</a></li>
               </ul>
           </div>

       </header>

        <section id="portfolio-detail">


            <div class="container">

                <div class="col-md-8">
                    <div class="image-portfolio">
                        <div class="nav-item">
                            <a href="#"><span class="right"></span></a>
                            <a href="#"><span class="left"></span></a>
                        </div>
                        <img src="http://40.media.tumblr.com/d2bebd55e7d06b6e05e4b3cab0e43d1d/tumblr_nsej67YjtF1qc6htko1_1280.jpg">
                        <img src="http://40.media.tumblr.com/aff4a2a4bd68dcc0ef60dc143290411f/tumblr_nrt64jXe1P1qc6htko1_1280.jpg">
                        <img src="http://41.media.tumblr.com/313f89a9f6270c256e2b2437d9945f7c/tumblr_nrnerusrDC1qc6htko1_1280.jpg">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="info-portfolio">
                        Nom del treball<br>
                        Identity<br>
                        —<br>
                        In such a visually polluted environment<br>
                        I try to come up with fresh and memorable designs with a clear aim at essential beauty and equilibrium that, at the same time, will ensure communicative effectiveness.
                    </div>
                    <div class="share">
                        <span>SHARE</span>
                        <a href="#">Facebook</a><br>
                        <a href="#">Twitter</a><br>
                        <a href="#">Pinterest</a><br>
                        <a href="#">Email</a>
                    </div>
                </div>

            </div>





        </section>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>

        <!-- bootstrap JS -->
        <script src="js/vendor/bootstrap/bootstrap.min.js"></script>
        <!-- Grid Loading -->
        <script src="js/vendor/grid-loading/masonry.pkgd.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <script>
            new AnimOnScroll( document.getElementById( 'grid' ), {
                minDuration : 0.4,
                maxDuration : 0.7,
                viewportFactor : 0.2
            } );
        </script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        -->
    </body>
</html>
