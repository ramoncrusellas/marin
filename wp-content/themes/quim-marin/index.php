<?php
/**
 * Quim Marin Template
 */

get_header(); ?>

        <section id="home">

            <div class="column-1">
                Art Direction & Design<br>
                Barcelona<br>
                —<br>
                In such a visually polluted environment<br>
                I try to come up with fresh and memorable designs with a clear aim at essential beauty and equilibrium that, at the same time, will ensure communicative effectiveness.
            </div>

            <div class="column-2">
                <p>
                    Branding<br>
                    Webdesign<br>
                    Packging<br>
                    Advertising<br>
                    Editorial
                </p>
                <p>
                    <a href="#">Behance</a><br>
                    <a href="#">Facebook</a><br>
                    <a href="#">Istagram</a><br>
                    <a href="#">Tumblr</a>
                </p>
            </div>
            <div class="clear"></div>

            <div class="image-home">
                <img src="<?php echo get_template_directory_uri(); ?>/img/content/portada.jpg" >
            </div>

        </section>



<?php get_footer(); ?>
