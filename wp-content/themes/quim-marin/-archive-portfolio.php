<?php

get_header(); ?>

        <section id="portfolio">

            <ul class="grid effect-2" id="grid">

                <?php
                the_post();


                $args = array(
                    'order' => 'DESC',
                    'post_type' => 'projects',
                    'taxonomy'  =>  array('category')
                );

                $projectes = new WP_Query($args);


                if ($projectes->post_count > 0) :

                $count = 0;
                while ($projectes->have_posts()) :
                    $projectes->the_post();

                    ?>

                    <li><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                            <div class="caption"></div>
                            <?php the_post_thumbnail(); ?>
                        </a>
                    </li>

                    <?php

                    $count++;

                endwhile;

                wp_reset_postdata();

                endif;  ?>


               <!-- <li><a href="portfolio-detail.php">
                        <div class="caption"></div>
                        <img src="http://40.media.tumblr.com/d2bebd55e7d06b6e05e4b3cab0e43d1d/tumblr_nsej67YjtF1qc6htko1_1280.jpg">
                    </a>
                </li>

                -->



            </ul>



        </section>



<?php get_footer(); ?>