<?php

get_header(); ?>

        <section id="portfolio">
            <ul class="grid effect-2" id="grid">
				<?php $category = get_category( get_query_var( 'cat' ) ) ?>
				
                <?php $args = array(
                    'order' => 'DESC',
                    'post_type' => 'projects',
                    'taxonomy'  =>  array('category'), 
                );

                $projectes = new WP_Query($args);

				if ($projectes->post_count > 0) :
	                $count = 0;
	                while ($projectes->have_posts()) :
	                    $projectes->the_post(); ?>

	                    <li>
	                    	<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
	                            <div class="caption">
                                    <div class="open">Open</div>
	                            </div>
	                            <?php the_post_thumbnail(); ?>
	                        </a>
	                    </li>

	                    <?php
	                    $count++;
	                endwhile;
                endif ?>

            </ul>
        </section>

<?php get_footer(); ?>