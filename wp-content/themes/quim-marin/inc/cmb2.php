<?php
/**
 * Include and setup custom metaboxes and fields.
 *
 * @package  planes_bones
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */
if ( ! class_exists( 'CMB2_Bootstrap_208', false ) && file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
}

/**
 * Display catering metabox only on catering pages
 */
function planes_bones_hide_if_not_category_page( $cmb ) {
	if ( "page-templates/catering-detail.php" != get_page_template_slug($cmb->object_id) ) {
		return false;
	}
	return true;
}

/**
 * Display galeria metabox only on galeria pages
 */
function planes_bones_hide_if_not_gallery_page( $cmb ) {
	if ( "page-templates/gallery.php" != get_page_template_slug($cmb->object_id) ) {
		return false;
	}
	return true;
}

/**
 * Display mosaic metabox only on mosaic pages
 */
function planes_bones_hide_if_not_mosaic_page( $cmb ) {
	if ( "page-templates/mosaic.php" != get_page_template_slug($cmb->object_id) ) {
		return false;
	}
	return true;
}

add_action( 'cmb2_init', 'planes_bones_register_header_metabox' );
/**
 * Hook in and add the header options metabox. Can only happen on the 'cmb2_init' hook.
 */
function planes_bones_register_header_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_planes_bones_';

	/**
	 * Catering metabox
	 */
	$cmb_header = new_cmb2_box( array(
		'id'            => $prefix . 'catering_metabox',
		'title'         => __( 'Catering', 'planes-bones' ),
		'object_types'  => array( 'page', ), // Post type
		'show_on_cb' => 'planes_bones_hide_if_not_category_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
	) );

	$cmb_header->add_field( array(
		'name'         => __( 'Slider', 'planes-bones' ),
		'desc'         => __( 'Afegir imatges', 'planes-bones' ),
		'id'           => $prefix . 'slider_files',
		'type'         => 'file_list',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );

	$cmb_header->add_field( array(
	    'name'    => '2a columna',
	    'id'      => $prefix . 'second_column',
	    'type'    => 'wysiwyg',
	    'options' => array( 
	    	'media_buttons' => false,
	    	'teeny' => false,
	    	'textarea_rows' => 8,
	    ),
	) );

	/**
	 * Galeria metabox
	 */
	$cmb_header = new_cmb2_box( array(
		'id'            => $prefix . 'galeria_metabox',
		'title'         => __( 'Galeria', 'planes-bones' ),
		'object_types'  => array( 'page', ), // Post type
		'show_on_cb' => 'planes_bones_hide_if_not_gallery_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
	) );

	$cmb_header->add_field( array(
	    'name'    => '2a columna',
	    'id'      => $prefix . 'second_column',
	    'type'    => 'wysiwyg',
	    'options' => array( 
	    	'media_buttons' => false,
	    	'teeny' => false,
	    	'textarea_rows' => 8,
	    ),
	) );

	/**
	 * Mosaic metabox
	 */
	$cmb_header = new_cmb2_box( array(
		'id'            => $prefix . 'mosaic_metabox',
		'title'         => __( 'Mosaic', 'planes-bones' ),
		'object_types'  => array( 'page', ), // Post type
		'show_on_cb' => 'planes_bones_hide_if_not_mosaic_page', // function should return a bool value
		// 'context'    => 'normal',
		// 'priority'   => 'high',
		// 'show_names' => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
	) );

	$cmb_header->add_field( array(
		'name'       => __( 'Subtítol 1a secció', 'cmb2' ),
		'id'         => $prefix . 'subtitle_first_column',
		'type'       => 'text',
	) );

	$cmb_header->add_field( array(
		'name'       => __( 'Subtítol 2a secció', 'cmb2' ),
		'id'         => $prefix . 'subtitle_second_column',
		'type'       => 'text',
	) );

	$cmb_header->add_field( array(
	    'name'    => '2a secció',
	    'id'      => $prefix . 'second_column',
	    'type'    => 'wysiwyg',
	    'options' => array( 
	    	'media_buttons' => false,
	    	'teeny' => false,
	    	'textarea_rows' => 8,
	    ),
	) );

	$cmb_header->add_field( array(
		'name'         => __( 'Mosaic', 'planes-bones' ),
		'desc'         => __( 'Afegir imatges', 'planes-bones' ),
		'id'           => $prefix . 'mosaic_files',
		'type'         => 'file_list',
		'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	) );

}
