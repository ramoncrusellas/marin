<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 */
?><!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Quim Marin Studio</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/includes/bootstrap/bootstrap.min.css">

        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/includes/grid-loading/component.css">

        <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.8.3.min.js"></script>

    </head>
    <body <?php body_class(); ?>>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


       <header>

           <a href="<?php echo get_site_url(); ?>"><div class="brand">Quim Marin<br>Studio</div></a>

           <div class="menu">
               <ul>
                   <?php if (in_category('8'))  { ?>
                       <li><a href="<?php echo get_site_url(); ?>/works/identity" class="identity active">Identity</a></li>
                   <?php } else {?>
                       <li><a href="<?php echo get_site_url(); ?>/works/identity" class="identity">Identity</a></li>
                   <?php } ?>

                   <?php if (in_category('9'))  { ?>
                       <li><a href="<?php echo get_site_url(); ?>/works/posters" class="posters active">Posters</a></li>
                   <?php } else {?>
                       <li><a href="<?php echo get_site_url(); ?>/works/posters" class="posters">Posters</a></li>
                   <?php } ?>

                   <?php if (in_category('10'))  { ?>
                       <li><a href="<?php echo get_site_url(); ?>/works/design" class="design active">Design</a></li>
                   <?php } else {?>
                       <li><a href="<?php echo get_site_url(); ?>/works/design" class="design">Design</a></li>
                   <?php } ?>
                   <li><a href="mailto:hola@quimmarin.com">Contact</a></li>
               </ul>
           </div>

           <div class="menu-mobile"></div>

           <div class="nav-menu-mobile">
               <ul>
                   <?php if (in_category('8'))  { ?>
                       <li><a href="<?php echo get_site_url(); ?>/works/identity" class="identity active">Identity</a></li>
                   <?php } else {?>
                       <li><a href="<?php echo get_site_url(); ?>/works/identity" class="identity">Identity</a></li>
                   <?php } ?>

                   <?php if (in_category('9'))  { ?>
                       <li><a href="<?php echo get_site_url(); ?>/works/posters" class="posters active">Posters</a></li>
                   <?php } else {?>
                       <li><a href="<?php echo get_site_url(); ?>/works/posters" class="posters">Posters</a></li>
                   <?php } ?>

                   <?php if (in_category('10'))  { ?>
                       <li><a href="<?php echo get_site_url(); ?>/works/design" class="design active">Design</a></li>
                   <?php } else {?>
                       <li><a href="<?php echo get_site_url(); ?>/works/design" class="design">Design</a></li>
                   <?php } ?>
                   <li><a href="mailto:hola@quimmarin.com">Contact</a></li>
               </ul>
           </div>

       </header>

