<?php
get_header(); ?>
        <section id="portfolio-detail">
            <div class="container" style="margin-left: 0px;">
                <div class="col-md-8">
                    <div class="image-portfolio">
                        <div class="nav-item">
                        <?php
                        	echo next_post_link('%link', '<span class="right"></span>', $in_same_cat = true);
                        	echo previous_post_link('%link', '<span class="left"></span>', $in_same_cat = true);
						?>
                        </div>
                        <?php the_post_thumbnail() ?>
                        <?php foreach(miu_get_images() as $image): ?>
                        	<div class="image-portfolio">
								<img src="<?php echo $image ?>">
								</div>
                        <?php endforeach ?>
                     
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="info-portfolio">
                        <?php the_title() ?><br>
                        <?php the_category(', ') ?><br>
                        —<br>
                        <?php echo $post->post_content; ?>
                    </div>
                    <div class="share">
                        <span>SHARE</span>
                        <a href="https://www.facebook.com/sharer/sharer.php?u<?php the_permalink() ?>" target="_blank">Facebook</a><br>
                        <a href="http://twitter.com/share?text=Quim Marin&url=<?php the_permalink() ?>&via=MarinQuim" target="_blank" >Twitter</a><br>
                        <a href="https://pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&media=<?php echo $image ?>&description=" target="_blank" >Pinterest</a><br>
                        <a href="mailto:?&subject=Quim Marin&body=<?php the_permalink() ?>">Email</a>

                    </div>
                </div>

            </div>
        </section>
<?php get_footer(); ?>